import * as yup from 'yup';
const USER_INPUT_VALIDATION = /^[a-zA-Z][a-zA0-9-_]{3,23}$/;
const PASSWORD_INPUT_VALIDATION = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/;
export const registerForm = yup.object().shape({
    username: yup.string().matches(USER_INPUT_VALIDATION, 'Invalid username. Username must start with a letter').required('Required'),
    pwd: yup
      .string()
      .min(8)
      .matches(PASSWORD_INPUT_VALIDATION, { message: " Invalid password. 8 to 24 characters. Must include uppercase and lowercase letters, a number, and a special character. Allowed special characters: ! @ # $ %" })
      .required("Required"),
      confirm_pwd: yup
      .string().min(8)
      .oneOf([yup.ref("pwd"), null], "Passwords must match").required("Required"),
      email:yup.string().email('Enter a valid email').required('Required')
  });
export const loginForm = yup.object().shape({
    login_user_name:yup.string().min(4, 'Invalid username. Username must start with a letter and at least 4 characters').required('Required'),
    login_pwd:yup.string().min(8,'Invalid password. 8 to 24 characters').required('Required')
})