import React from "react";
import { Form, Button, Container, Row, Col, Card } from "react-bootstrap";
import { useRef, useEffect } from "react";
import { useFormik } from "formik";
import { Link } from "react-router-dom";
import { loginForm } from "../schemas/formSchema";
function Login() {
  const { errors, values, handleChange, getFieldProps, handleSubmit } =
    useFormik({
      initialValues: {
        login_user_name: "",
        login_pwd: "",
        email: "",
      },
      validationSchema: loginForm,
    });
  return (
    <Container>
      <Row>
        <Col className="mt-5 ">
          <h3 className="form-logo ps-2">Sign In</h3>
          <form
            className="d-flex flex-column justify-content-start form-style fw-bold"
            onSubmit={handleSubmit}
          >
            <div className="d-flex flex-column mb-3">
              {errors.login_user_name ? (
                <label className="text-danger mb-1">User Name</label>
              ) : (
                <label className="text-success mb-1">User Name</label>
              )}
              <input
                type="text"
                value={values.login_user_name}
                name="login_user_name"
                {...getFieldProps("login_user_name")}
                onChange={handleChange}
              />
              {errors.login_user_name && (
                <p className="input-error px-2 py-1">
                  {errors.login_user_name}
                </p>
              )}
            </div>
            <div className="d-flex flex-column mb-3">
              {errors.login_pwd ? (
                <label className="text-danger mb-1">Password</label>
              ) : (
                <label className="text-success mb-1">Password</label>
              )}
              <input
                type="password"
                value={values.login_pwd}
                name="login_pwd"
                {...getFieldProps("login_pwd")}
                onChange={handleChange}
              />
              {errors.login_pwd && (
                <p className="input-error px-2 py-1">{errors.login_pwd}</p>
              )}
            </div>
            <button className="mt-5 mx-auto log-in-btn px-4 fw-bold text-white py-2">
              Submit
            </button>
            <div className="mt-3">
              <span>
                Don't have an account?{" "}
                <Link className="text-decoration-none" to="/register">
                  Click
                </Link>
              </span>
            </div>
          </form>
        </Col>
      </Row>
    </Container>
  );
}

export default Login;
