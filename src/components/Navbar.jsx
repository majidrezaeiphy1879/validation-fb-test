import React from 'react'
import { Link } from 'react-router-dom'

function Navbar() {
  return  (
    <div className='d-flex justify-content-end px-4 py-4 bg-success '>
      <Link className='navbar-style me-5' to='/register'>Sign Up</Link>
      <Link className='navbar-style ' to='/login'>Log In</Link>
    </div>
  )
}

export default Navbar