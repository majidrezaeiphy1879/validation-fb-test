import React, { useState } from "react";
import { Form, Button, Container, Row, Col, Spinner } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import axios from "axios";
import { registerForm } from "../schemas/formSchema";

function Signup() {
  const navigate = useNavigate();
  const [registrationSuccess, setRegistrationSuccess] = useState(false);
  const onSubmit = async (values, actions) => {
    try {
      setRegistrationSuccess(true);

      actions.resetForm();

      setTimeout(() => {
        navigate("/login");
      }, 3000);
    } catch (err) {
      console.log(err);
    }
  };
  const {
    touched,
    handleChange,
    handleSubmit,
    errors,
    isSubmitting,
    values,
    actions,
    getFieldProps,
    handleBlur,
    resetForm,
  } = useFormik({
    initialValues: {
      username: "",
      pwd: "",
      confirm_pwd: "",
      email: "",
    },
    onSubmit,
    validationSchema: registerForm,
  });
//  console.log(errors, actions)
  return (
    <Container>
      <Row >
        <Col className="mt-5 ">
          {registrationSuccess ? (
            <>
              <Col className="mt-5 d-flex justify-content-center align-items-center  successfull-register p-5 ">
                <h3 className="text-primary ">Registeration successfull. In a few seconds you will be navigated to the login page</h3>
              </Col>
            </>
          ) : (
            <>
              <h3 className="form-logo ps-2">Sign Up</h3>
              <form
                onSubmit={handleSubmit}
                className="d-flex flex-column justify-content-start form-style fw-bold "
              >
                <div className="d-flex flex-column mb-3">
                  {errors.username ? (
                    <label className="text-danger mb-1">User Name</label>
                  ) : (
                    <label className="text-success mb-1">User Name</label>
                  )}
                  <input
                    type="text"
                    name="username"
                    value={values.username}
                    {...getFieldProps("username")}
                    onChange={handleChange}
                  />
                  {errors.username && touched.username ? (
                    <p className="input-error px-2 py-1">{errors.username}</p>
                  ) : null}
                </div>

                <div className="d-flex flex-column mb-3">
                  {errors.email ? (
                    <label className="text-danger mb-1">Email</label>
                  ) : (
                    <label className="text-success mb-1">Email</label>
                  )}
                  <input
                    type="email"
                    name="email"
                    value={values.email}
                    {...getFieldProps("email")}
                    onChange={handleChange}
                  />
                  {errors.email && touched.email ? (
                    <p className="input-error px-2 py-1">{errors.email}</p>
                  ) : null}
                </div>

                <div className="d-flex flex-column mb-3">
                  {errors.pwd ? (
                    <label className="text-danger mb-1">Password</label>
                  ) : (
                    <label className="text-success mb-1">Password</label>
                  )}
                  <input
                    type="password"
                    name="pwd"
                    value={values.pwd}
                    {...getFieldProps("pwd")}
                    onChange={handleChange}
                  />
                  {errors.pwd && touched.pwd ? (
                    <p className="input-error px-2 py-1">{errors.pwd}</p>
                  ) : null}
                </div>
                <div className="d-flex flex-column">
                  {errors.confirm_pwd ? (
                    <label className="text-danger mb-1">Confirm Password</label>
                  ) : (
                    <label className="text-success mb-1">
                      Confirm Password
                    </label>
                  )}
                  <input
                    type="password"
                    name="confirm_pwd"
                    onChange={handleChange}
                    {...getFieldProps("confirm_pwd")}
                    value={values.confirm_pwd}
                  />
                  {errors.confirm_pwd && touched.confirm_pwd ? (
                    <p className="input-error px-2 py-1">
                      {errors.confirm_pwd}
                    </p>
                  ) : null}
                </div>

                {errors.confirm_pwd || errors.pwd || errors.username ? (
                  <button
                    className="error-btn mt-4 mx-auto py-2 px-4 text-white fw-bold "
                    disabled
                  >
                    Can't Submit
                  </button>
                ) : isSubmitting ? (
                  <button
                    className="mt-5 mx-auto no-error-btn py-2 px-4 fw-bold text-white"
                    disabled
                  >
                    <Spinner animation="border" />
                  </button>
                ) : (
                  <button className="mt-5 mx-auto no-error-btn py-2 px-4 fw-bold text-white">
                    Submit
                  </button>
                )}
              </form>
            </>
          )}
        </Col>
      </Row>
    </Container>
  );
}

export default Signup;
