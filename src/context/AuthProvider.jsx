import React, { createContext, useReducer } from "react";

const initialState = {  };

const authReducer = (state, action) => {
  switch (action.type) {
    case "SET_AUTH":
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [auth, dispatch] = useReducer(authReducer, initialState);

  const setAuth = (newAuth) => {
    dispatch({ type: "SET_AUTH", payload: newAuth });
  };

  return (
    <AuthContext.Provider value={{ auth, setAuth }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
